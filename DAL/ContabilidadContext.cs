using DomainModel;
using System.Data.Entity;

namespace DAL
{

    public class ContabilidadContext : DbContext
    {
        public ContabilidadContext()
            : base("name=ContabilidadEntities")
        {

        }

        public virtual DbSet<Auxiliar> Auxiliar { get; set; }
        public virtual DbSet<CuentaContable> CuentaContable { get; set; }
        public virtual DbSet<DetalleEntrada> DetalleEntrada { get; set; }
        public virtual DbSet<EntradaContable> EntradaContable { get; set; }
        public virtual DbSet<Mayorizacion> Mayorizacion { get; set; }
        public virtual DbSet<TipoCuenta> TipoCuenta { get; set; }
        public virtual DbSet<TipoMoneda> TipoMoneda { get; set; }
        public virtual DbSet<TipoMovimiento> TipoMovimiento { get; set; }
        public virtual DbSet<Rol> Rol { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Auxiliar>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<CuentaContable>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<CuentaContable>()
                .Property(e => e.NoCuenta)
                .IsUnicode(false);

            modelBuilder.Entity<EntradaContable>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<TipoCuenta>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<TipoCuenta>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<TipoMoneda>()
                .Property(e => e.Formato)
                .IsUnicode(false);

            modelBuilder.Entity<TipoMoneda>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<TipoMovimiento>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<CuentaContable>()
                .HasOptional(t => t.CuentaMayor);
        }
    }
}
