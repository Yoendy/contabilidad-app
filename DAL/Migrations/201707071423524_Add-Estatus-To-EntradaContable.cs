namespace DAL.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddEstatusToEntradaContable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EntradaContable", "Estatus", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EntradaContable", "Estatus");
        }
    }
}
