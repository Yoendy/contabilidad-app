﻿using System.Collections.Generic;
using DomainModel;
using Service.Common;

namespace Service.TiposCuenta
{
    public interface ITipoCuentaService : IService<TipoCuenta>
    {
        IEnumerable<TipoCuenta> Search(string filtro);
    }
}
