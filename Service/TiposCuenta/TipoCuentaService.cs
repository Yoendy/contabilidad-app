﻿using System.Collections.Generic;
using DomainModel;
using FluentValidation;
using Service.Common;
using Service.Validations;

namespace Service.TiposCuenta
{
    public class TipoCuentaService : ServiceBase<TipoCuenta>, ITipoCuentaService 
    {
        public IEnumerable<TipoCuenta> Search(string filtro)
        {
            return GetAll(x => x.Id.ToString().StartsWith(filtro) || x.Nombre.Contains(filtro) ||
                               x.Descripcion.Contains(filtro) || x.Origen.Nombre.Equals(filtro));
        }

        public override void Save(TipoCuenta entity)
        {
            var tipoCuentaValidator = new TipoCuentaValidator();
            tipoCuentaValidator.ValidateAndThrow(entity);
            base.Save(entity);
        }
    }
}
