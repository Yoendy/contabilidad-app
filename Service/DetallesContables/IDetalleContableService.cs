﻿using System.Collections.Generic;
using DomainModel;
using Service.Common;

namespace Service.DetallesContables
{
    public interface IDetalleContableService : IService<DetalleEntrada>
    {
        IEnumerable<DetalleEntrada> GetAllByEntrada(int entradaId);
    }
}
