﻿using System;
using System.Linq;
using DomainModel;
using FluentValidation;

namespace Service.Validations
{
    public class EntradaContableValidator : AbstractValidator<EntradaContable>
    {
        public EntradaContableValidator()
        {
            RuleFor(item => item.Descripcion).NotEmpty().WithMessage("La descripcion es obligatoria.")
                .MaximumLength(100).WithMessage("La descripcion no puede tener mas de 100 caracteres.");

            RuleFor(item => item.Detalles).Must(list => list.Count >= 1).WithMessage("El asiento debe afectar por lo menos 2 cuentas.")
                 .SetCollectionValidator(new DetalleContableValidator());

            RuleFor(item => item.Fecha).Must(IsValidDate).WithMessage("La fecha es obligatoria.");

            RuleFor(item => item).Must(IsDebitSameThatCredit).WithMessage("El debito es diferente del credito.");

            RuleFor(item => item).Must(IsValidAsiento).WithMessage("No se pueden debitar o acreditar la misma cuenta.");

        }

        private bool IsValidDate(DateTime date)
        {
            return !date.Equals(default(DateTime));
        }

        private bool IsDebitSameThatCredit(EntradaContable entrada)
        {
            return entrada.Detalles.Sum(x => x.Credito) == entrada.Detalles.Sum(y => y.Debito);
        }

        private bool IsValidAsiento(EntradaContable entrada)
        {
            return entrada.Detalles.Select(x => x.CuentaId).Distinct().Count() == entrada.Detalles.Count();
        }
    }
}
