﻿using DomainModel;
using FluentValidation;

namespace Service.Validations
{
    public class TipoCuentaValidator : AbstractValidator<TipoCuenta>
    {
        public TipoCuentaValidator()
        {
            RuleFor(item => item.Nombre).NotEmpty().WithMessage("El nombre es obligatorio.")
                                        .MaximumLength(100).WithMessage("El nombre no puede tener mas de 100 caracteres.");

            RuleFor(item => item.Descripcion).NotEmpty().WithMessage("La descripcion es obligatoria.")
                                        .MaximumLength(100).WithMessage("La descripcion no puede tener mas de 100 caracteres.");


        }
    }
}
