﻿using DomainModel;
using FluentValidation;
using Service.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Validations
{
    public class UsuarioValidator : AbstractValidator<Usuario>
    {
        public UsuarioValidator()
        {
            RuleFor(item => item.UserName).NotEmpty().WithMessage("El usuario es obligatorio.");
                            
            RuleFor(item => item.Nombre).NotEmpty().WithMessage("El nombre es obligatorio.");
                                           

            RuleFor(item => item.Clave).NotEmpty().WithMessage("La clave es obligatoria.");

            RuleFor(item => item).Must(IsValidUserName).WithMessage("El usuario deber ser unico.");
        }

        private bool IsValidUserName(Usuario usuario)
        {
            var isValid = false;
            var _usuarioService = new UsuarioService();
            if(usuario.Id != 0)
            {
                isValid = !_usuarioService.Exists(x => x.UserName.Equals(usuario.UserName) && x.Id != usuario.Id);
            }
            else
            {
                isValid = !_usuarioService.Exists(x => x.UserName.Equals(usuario.UserName));
            }
            return isValid;
        }
    }
}
