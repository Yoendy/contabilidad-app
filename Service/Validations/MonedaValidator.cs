﻿using DomainModel;
using FluentValidation;

namespace Service.Validations
{
    public class MonedaValidator : AbstractValidator<TipoMoneda>
    {
        public MonedaValidator()
        {
            RuleFor(item => item.Descripcion).NotEmpty().WithMessage("La descripcion es obligatoria.")
                                             .MaximumLength(100).WithMessage("La descripcion no puede tener mas de 100 caracteres."); 

            RuleFor(item => item.Formato).NotEmpty().WithMessage("El formato es obligatorio.")
                                            .MaximumLength(100).WithMessage("El formato no puede tener mas de 100 caracteres.");

            RuleFor(item => item.TasaActual).NotEmpty().WithMessage("La tasa es obligatoria.");
            
        }
    }
}
