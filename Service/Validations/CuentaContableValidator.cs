﻿using DomainModel;
using FluentValidation;

namespace Service.Validations
{
    public class CuentaContableValidator : AbstractValidator<CuentaContable>
    {
        public CuentaContableValidator()
        {
            RuleFor(item => item.Descripcion).NotEmpty().WithMessage("La descripcion es obligatoria.")
                .MaximumLength(100).WithMessage("La descripcion no puede tener mas de 100 caracteres.");

            RuleFor(item => item.NoCuenta).NotEmpty().WithMessage("El no de cuenta es obligatorio.")
                .MaximumLength(50).WithMessage("El formato no puede tener mas de 50 caracteres.");

            //RuleFor(item => item.PermiteTransacciones).NotEmpty().WithMessage("Se debe definir si la cuenta permite transacciones.");
        }
    }
}
