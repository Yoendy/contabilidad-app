﻿using DomainModel;
using FluentValidation;

namespace Service.Validations
{
    public  class DetalleContableValidator : AbstractValidator<DetalleEntrada>
    {
        public DetalleContableValidator()
        {
            RuleFor(item => item.CuentaId).NotNull().WithMessage("La cuenta contable es obligatoria.");

            RuleFor(item => item.Credito).NotNull().WithMessage("El monto credito es obligatorio.");
                                         //.LessThan(0).WithMessage("El monto credito debe ser positivo.");

            RuleFor(item => item.Debito).NotNull().WithMessage("El monto debito es obligatorio.");
            //.LessThan(0).WithMessage("El monto debito debe ser positivo.");
        }
    }
}
