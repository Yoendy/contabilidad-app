﻿using System.Collections.Generic;
using DomainModel;
using Service.Common;

namespace Service.EntradasContables
{
    public interface IEntradaContableService : IService<EntradaContable>
    {
        IEnumerable<EntradaContable> Search(string filtro);
    }
}
