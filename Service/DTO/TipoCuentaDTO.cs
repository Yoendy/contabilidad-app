﻿namespace Service.DTO
{
    public class TipoCuentaDto
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int Origen { get; set; } = 1;
        public int NoControl { get; set; } = 1;
        public bool Estado { get; set; } = true;
    }
}
