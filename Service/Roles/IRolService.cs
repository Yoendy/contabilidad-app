﻿using DomainModel;
using Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Roles
{
    interface IRolService : IService<Rol>
    {
    }
}
