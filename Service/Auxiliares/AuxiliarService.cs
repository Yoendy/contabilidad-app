﻿using System.Collections.Generic;
using DomainModel;
using FluentValidation;
using Service.Common;
using Service.Validations;

namespace Service.Auxiliares
{
    public class AuxiliarService : ServiceBase<Auxiliar>,IAuxiliarService
    {
        public IEnumerable<Auxiliar> Search(string filtro)
        {
            return GetAll(x => x.Descripcion.StartsWith(filtro));
        }

        public override void Save(Auxiliar entity)
        {
            var validator = new AuxiliarValidator();
            validator.ValidateAndThrow(entity);
            base.Save(entity);
        }
    }
}
