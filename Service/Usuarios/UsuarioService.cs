﻿using DomainModel;
using FluentValidation;
using Service.Common;
using Service.Validations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Usuarios
{
    public class UsuarioService : ServiceBase<Usuario>, IUsuarioService
    {
        public bool Login(string UserName, string Clave)
        {
            if (string.IsNullOrEmpty(UserName)) throw new Exception("El usuario es obligatorio.");

            if (string.IsNullOrEmpty(Clave)) throw new Exception("La clave es obligatoria.");

            return Exists(x => x.UserName.Equals(UserName) && x.Clave.Equals(Clave) && x.Estado);
        }

        public IEnumerable<Usuario> Search(string filtro)
        {
            return GetAll(x => x.UserName.Contains(filtro) || x.Nombre.Contains(filtro) || x.Rol.Nombre.Contains(filtro));
        }

        public override void Save(Usuario entity)
        {
            var usuarioValidator = new UsuarioValidator();
            usuarioValidator.ValidateAndThrow(entity);
            base.Save(entity);
        }

    }
}
