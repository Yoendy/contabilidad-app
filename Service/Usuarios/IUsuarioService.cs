﻿using DomainModel;
using Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Usuarios
{
    public interface IUsuarioService : IService<Usuario>
    {
        bool Login(string UserName, string Clave);

        IEnumerable<Usuario> Search(string filtro);
    }
}
