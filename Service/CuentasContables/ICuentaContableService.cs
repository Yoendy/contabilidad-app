﻿using System.Collections.Generic;
using DomainModel;
using Service.Common;

namespace Service.CuentasContables
{
    public interface ICuentaContableService : IService<CuentaContable>
    {
        IEnumerable<CuentaContable> Search(string filtro);
    }
}
