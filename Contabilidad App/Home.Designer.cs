﻿using Contabilidad_App.Utils;

namespace Contabilidad_App
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.tipoCuenta1 = new TipoCuentas.TipoCuenta();
            this.metroTabPage2 = new MetroFramework.Controls.MetroTabPage();
            this.cuentaContable1 = new CuentasContables.CuentaContable();
            this.metroTabPage3 = new MetroFramework.Controls.MetroTabPage();
            this.moneda1 = new TipoMonedas.Moneda();
            this.metroTabPage4 = new MetroFramework.Controls.MetroTabPage();
            this.auxiliar1 = new Auxiliares.Auxiliar();
            this.metroTabPage5 = new MetroFramework.Controls.MetroTabPage();
            this.asiento1 = new EntradasContable.Asiento();
            this.metroTabPage6 = new MetroFramework.Controls.MetroTabPage();
            this.balanceComprobacion1 = new Reports.BalanceDeComprobacion();
            this.metroTabPage7 = new MetroFramework.Controls.MetroTabPage();
            this.usuario1 = new Usuarios.Usuarios();
            this.metroTabControl1.SuspendLayout();
            this.metroTabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));

            this.metroTabControl1.Controls.Add(this.metroTabPage5);
            this.metroTabControl1.Controls.Add(this.metroTabPage3);
            this.metroTabControl1.Controls.Add(this.metroTabPage6);

            this.metroTabControl1.Location = new System.Drawing.Point(20, 60);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 0;
            this.metroTabControl1.Size = new System.Drawing.Size(780, 345);
            this.metroTabControl1.TabIndex = 0;
            this.metroTabControl1.UseSelectable = true;

            if(UsuarioActual.Usuario.Rol.Id == 1)
            {
                // 
                // metroTabControl1
                // 
                this.metroTabControl1.Controls.Clear();
                this.metroTabControl1.Controls.Add(this.metroTabPage1);
                this.metroTabControl1.Controls.Add(this.metroTabPage2);
                this.metroTabControl1.Controls.Add(this.metroTabPage3);
                this.metroTabControl1.Controls.Add(this.metroTabPage4);
                this.metroTabControl1.Controls.Add(this.metroTabPage5);
                this.metroTabControl1.Controls.Add(this.metroTabPage6);
                this.metroTabControl1.Controls.Add(this.metroTabPage7);
                // 
                // metroTabPage1
                // 
                this.metroTabPage1.Controls.Add(this.tipoCuenta1);
                this.metroTabPage1.HorizontalScrollbarBarColor = true;
                this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = false;
                this.metroTabPage1.HorizontalScrollbarSize = 10;
                this.metroTabPage1.Location = new System.Drawing.Point(4, 38);
                this.metroTabPage1.Name = "metroTabPage1";
                this.metroTabPage1.Size = new System.Drawing.Size(772, 303);
                this.metroTabPage1.TabIndex = 0;
                this.metroTabPage1.Text = "Tipos Cuentas";
                this.metroTabPage1.VerticalScrollbarBarColor = true;
                this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
                this.metroTabPage1.VerticalScrollbarSize = 10;
                // 
                // tipoCuenta1
                // 
                this.tipoCuenta1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                | System.Windows.Forms.AnchorStyles.Left)
                | System.Windows.Forms.AnchorStyles.Right)));
                this.tipoCuenta1.AutoSize = true;
                this.tipoCuenta1.Location = new System.Drawing.Point(-4, 3);
                this.tipoCuenta1.Name = "tipoCuenta1";
                this.tipoCuenta1.Size = new System.Drawing.Size(783, 317);
                this.tipoCuenta1.TabIndex = 2;
                this.tipoCuenta1.UseSelectable = true;
                // 
                // metroTabPage2
                // 
                this.metroTabPage2.Controls.Add(this.cuentaContable1);
                this.metroTabPage2.HorizontalScrollbarBarColor = true;
                this.metroTabPage2.HorizontalScrollbarHighlightOnWheel = false;
                this.metroTabPage2.HorizontalScrollbarSize = 10;
                this.metroTabPage2.Location = new System.Drawing.Point(4, 38);
                this.metroTabPage2.Name = "metroTabPage2";
                this.metroTabPage2.Size = new System.Drawing.Size(772, 303);
                this.metroTabPage2.TabIndex = 1;
                this.metroTabPage2.Text = "Cuentas Contables";
                this.metroTabPage2.VerticalScrollbarBarColor = true;
                this.metroTabPage2.VerticalScrollbarHighlightOnWheel = false;
                this.metroTabPage2.VerticalScrollbarSize = 10;
                // 
                // cuentaContable1
                // 
                this.cuentaContable1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                                                                                 | System.Windows.Forms.AnchorStyles.Left)
                                                                                | System.Windows.Forms.AnchorStyles.Right)));
                this.cuentaContable1.AutoSize = true;
                this.cuentaContable1.Location = new System.Drawing.Point(-4, 3);
                this.cuentaContable1.Name = "cuentaContable1";
                this.cuentaContable1.Size = new System.Drawing.Size(783, 317);
                this.cuentaContable1.TabIndex = 2;
                this.cuentaContable1.UseSelectable = true;
                // 
                // metroTabPage4
                // 
                this.metroTabPage4.Controls.Add(this.auxiliar1);
                this.metroTabPage4.HorizontalScrollbarBarColor = true;
                this.metroTabPage4.HorizontalScrollbarHighlightOnWheel = false;
                this.metroTabPage4.HorizontalScrollbarSize = 10;
                this.metroTabPage4.Location = new System.Drawing.Point(4, 38);
                this.metroTabPage4.Name = "metroTabPage4";
                this.metroTabPage4.Size = new System.Drawing.Size(772, 303);
                this.metroTabPage4.TabIndex = 3;
                this.metroTabPage4.Text = "Auxiliares";
                this.metroTabPage4.VerticalScrollbarBarColor = true;
                this.metroTabPage4.VerticalScrollbarHighlightOnWheel = false;
                this.metroTabPage4.VerticalScrollbarSize = 10;

                // 
                // auxiliar1
                // 
                this.auxiliar1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                                                                             | System.Windows.Forms.AnchorStyles.Left)
                                                                            | System.Windows.Forms.AnchorStyles.Right)));
                this.auxiliar1.AutoSize = true;
                this.auxiliar1.Location = new System.Drawing.Point(-4, 3);
                this.auxiliar1.Name = "auxiliar1";
                this.auxiliar1.Size = new System.Drawing.Size(783, 317);
                this.auxiliar1.TabIndex = 2;
                this.auxiliar1.UseSelectable = true;

                // 
                // metroTabPage7
                // 
                this.metroTabPage7.Controls.Add(this.usuario1);
                this.metroTabPage7.HorizontalScrollbarBarColor = true;
                this.metroTabPage7.HorizontalScrollbarHighlightOnWheel = false;
                this.metroTabPage7.HorizontalScrollbarSize = 10;
                this.metroTabPage7.Location = new System.Drawing.Point(4, 38);
                this.metroTabPage7.Name = "metroTabPage7";
                this.metroTabPage7.Size = new System.Drawing.Size(772, 303);
                this.metroTabPage7.TabIndex = 3;
                this.metroTabPage7.Text = "Usuarios";
                this.metroTabPage7.VerticalScrollbarBarColor = true;
                this.metroTabPage7.VerticalScrollbarHighlightOnWheel = false;
                this.metroTabPage7.VerticalScrollbarSize = 10;

                // 
                // usuario1
                // 
                this.usuario1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                                                                             | System.Windows.Forms.AnchorStyles.Left)
                                                                            | System.Windows.Forms.AnchorStyles.Right)));
                this.usuario1.AutoSize = true;
                this.usuario1.Location = new System.Drawing.Point(-4, 3);
                this.usuario1.Name = "usuario1";
                this.usuario1.Size = new System.Drawing.Size(783, 317);
                this.usuario1.TabIndex = 2;
                this.usuario1.UseSelectable = true;
            }

            // 
            // metroTabPage3
            // 
            this.metroTabPage3.Controls.Add(this.moneda1);
            this.metroTabPage3.HorizontalScrollbarBarColor = true;
            this.metroTabPage3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.HorizontalScrollbarSize = 10;
            this.metroTabPage3.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage3.Name = "metroTabPage3";
            this.metroTabPage3.Size = new System.Drawing.Size(772, 303);
            this.metroTabPage3.TabIndex = 2;
            this.metroTabPage3.Text = "Tipos Monedas";
            this.metroTabPage3.VerticalScrollbarBarColor = true;
            this.metroTabPage3.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.VerticalScrollbarSize = 10;
            // 
            // moneda1
            // 
            this.moneda1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                                                                             | System.Windows.Forms.AnchorStyles.Left)
                                                                            | System.Windows.Forms.AnchorStyles.Right)));
            this.moneda1.AutoSize = true;
            this.moneda1.Location = new System.Drawing.Point(-4, 3);
            this.moneda1.Name = "moneda1";
            this.moneda1.Size = new System.Drawing.Size(783, 317);
            this.moneda1.TabIndex = 2;
            this.moneda1.UseSelectable = true;

            // 
            // metroTabPage5
            // 
            this.metroTabPage5.Controls.Add(this.asiento1);
            this.metroTabPage5.HorizontalScrollbarBarColor = true;
            this.metroTabPage5.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage5.HorizontalScrollbarSize = 10;
            this.metroTabPage5.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage5.Name = "metroTabPage5";
            this.metroTabPage5.Size = new System.Drawing.Size(772, 303);
            this.metroTabPage5.TabIndex = 4;
            this.metroTabPage5.Text = "Entradas Contables";
            this.metroTabPage5.VerticalScrollbarBarColor = true;
            this.metroTabPage5.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage5.VerticalScrollbarSize = 10;

            // 
            // asiento1
            // 
            this.asiento1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                                                                           | System.Windows.Forms.AnchorStyles.Left)
                                                                          | System.Windows.Forms.AnchorStyles.Right)));
            this.asiento1.AutoSize = true;
            this.asiento1.Location = new System.Drawing.Point(-4, 3);
            this.asiento1.Name = "asiento1";
            this.asiento1.Size = new System.Drawing.Size(783, 317);
            this.asiento1.TabIndex = 2;
            this.asiento1.UseSelectable = true;

            // 
            // metroTabPage6
            // 
            this.metroTabPage6.Controls.Add(this.balanceComprobacion1);
            this.metroTabPage6.HorizontalScrollbarBarColor = true;
            this.metroTabPage6.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage6.HorizontalScrollbarSize = 10;
            this.metroTabPage6.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage6.Name = "metroTabPage6";
            this.metroTabPage6.Size = new System.Drawing.Size(772, 303);
            this.metroTabPage6.TabIndex = 5;
            this.metroTabPage6.Text = "Reporte de Comprobacion";
            this.metroTabPage6.VerticalScrollbarBarColor = true;
            this.metroTabPage6.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage6.VerticalScrollbarSize = 10;

            // 
            // balanceComprobacion1
            // 
            this.balanceComprobacion1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                                                                           | System.Windows.Forms.AnchorStyles.Left)
                                                                          | System.Windows.Forms.AnchorStyles.Right)));
            this.balanceComprobacion1.AutoSize = true;
            this.balanceComprobacion1.Location = new System.Drawing.Point(-4, 3);
            this.balanceComprobacion1.Name = "asiento1";
            this.balanceComprobacion1.Size = new System.Drawing.Size(783, 317);
            this.balanceComprobacion1.TabIndex = 2;
            this.balanceComprobacion1.UseSelectable = true;

            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(820, 425);
            this.Controls.Add(this.metroTabControl1);
            this.Name = "Home";
            this.Text = "Sistema Contabilidad";
            this.Load += new System.EventHandler(this.Home_Load);
            this.metroTabControl1.ResumeLayout(false);
            this.metroTabPage1.ResumeLayout(false);
            this.metroTabPage1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage metroTabPage2;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private MetroFramework.Controls.MetroTabPage metroTabPage3;
        private MetroFramework.Controls.MetroTabPage metroTabPage4;
        private MetroFramework.Controls.MetroTabPage metroTabPage5;
        private MetroFramework.Controls.MetroTabPage metroTabPage6;
        private MetroFramework.Controls.MetroTabPage metroTabPage7;
        private TipoCuentas.TipoCuenta tipoCuenta1;
        private TipoMonedas.Moneda moneda1;
        private Auxiliares.Auxiliar auxiliar1;
        private CuentasContables.CuentaContable cuentaContable1;
        private EntradasContable.Asiento asiento1;
        private Reports.BalanceDeComprobacion balanceComprobacion1;
        private Usuarios.Usuarios usuario1;
    }
}

