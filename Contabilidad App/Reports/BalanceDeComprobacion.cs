﻿using System;
using System.Data;
using System.Linq;
using MetroFramework.Controls;
using Microsoft.Reporting.WinForms;
using Service.DetallesContables;

namespace Contabilidad_App.Reports
{
    public partial class BalanceDeComprobacion : MetroUserControl
    {
        public BalanceDeComprobacion()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            var fechaInicio = txtFechaInicio.Value.Date;
            var fechaTermino = txtFechaTermino.Value.Date;

            reportViewer1.ProcessingMode = ProcessingMode.Local;

            var report = reportViewer1.LocalReport;
            report.ReportPath = "Reports/BalanceComprobacionA.rdlc";

            var data = GetBalance(fechaInicio, fechaTermino);
            var dataSource = new ReportDataSource("DataSet1", data);

            report.DataSources.Clear();
            report.DataSources.Add(dataSource);

            var fechaInicial = new ReportParameter("FechaInicio", fechaInicio.ToShortDateString());
            var fechaFinal = new ReportParameter("FechaTermino", fechaTermino.ToShortDateString());

            report.SetParameters(new[] { fechaInicial, fechaFinal });

            report.Refresh();
            reportViewer1.RefreshReport();
        }

        private IQueryable<object> GetBalance(DateTime fechaInicio, DateTime fechaTermino)
        {
            var data = new DetalleContableService().Query(x => x.EntradaContable.Fecha >= fechaInicio && x.EntradaContable.Fecha <= fechaTermino)
                                                   .GroupBy(x => x.Cuenta.Descripcion)
                                                   .Select(x => new { Descripcion = x.Key, Debito = x.Sum(t => t.Debito), Credito = x.Sum(t => t.Credito) });
            return data;
        }

    }
}
