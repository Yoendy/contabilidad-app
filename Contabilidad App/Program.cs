﻿using System;
using System.Windows.Forms;
using Contabilidad_App.Reports;
using Contabilidad_App.Usuarios;

namespace Contabilidad_App
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Login());
        }
    }
}
