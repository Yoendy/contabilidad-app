﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Contabilidad_App.TipoMovimientos;
using MetroFramework.Controls;

namespace Contabilidad_App
{
    public partial class Home : MetroForm
    {
        public Home()
        {
            InitializeComponent();
        }

        public void AddUserControl(TabPage tabPage, MetroUserControl userControl)
        {
            userControl.Dock = DockStyle.Fill;
            tabPage.Controls.Add(userControl);
        }

        private void Home_Load(object sender, EventArgs e)
        {

        }
    }
}
