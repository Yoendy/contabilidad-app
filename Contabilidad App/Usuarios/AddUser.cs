﻿using DomainModel;
using MetroFramework;
using MetroFramework.Forms;
using Service.Roles;
using Service.Usuarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contabilidad_App.Usuarios
{
    public partial class AddUser : MetroForm
    {
        public Usuario Usuario { get; set; }
        private readonly IUsuarioService _usuarioService;

        public AddUser()
        {
            InitializeComponent();
            _usuarioService = new UsuarioService();
            Cargar();
        }

        private void Cargar()
        {
            CargarEstado();
            CargarRoles();
        }

        private void CargarEstado()
        {
            var estados = new Dictionary<bool, string> { { true, "Activo" }, { false, "Inactivo" } };
            cbxEstado.DataSource = new BindingSource(estados, null);
            cbxEstado.DisplayMember = "Value";
            cbxEstado.ValueMember = "Key";
        }

        private void CargarRoles()
        {
            var _rolService = new RolService();
            var roles = _rolService.GetAll().Select(x => new { x.Id, x.Nombre});
            cbxRol.DataSource = new BindingSource(roles, null);
            cbxRol.DisplayMember = "Nombre";
            cbxRol.ValueMember = "Id";
        }

        #region CRUD
        private void Add(Usuario model)
        {
            _usuarioService.Save(model);
        }
        #endregion

        #region Action

        private void Agregar()
        {
            var usuario = Map;
            Add(usuario);

        }

        private void Inactivar()
        {
            var tipoCuenta = Map;
            _usuarioService.Delete(tipoCuenta.Id);
        }

        private Usuario Map
        {
            get
            {
                var usuario = new Usuario
                {
                    Id = int.Parse(txtId.Text),
                    UserName = txtUsuario.Text,
                    Nombre = txtNombre.Text,
                    Clave = txtClave.Text,
                    RolId = (int)(cbxRol.SelectedValue),
                    Estado = (bool)cbxEstado.SelectedValue
                };
                return usuario;
            }
        }

        private void ReverseMap()
        {
            if (Usuario == null) return;

            txtId.Text = Usuario.Id.ToString();
            txtUsuario.Text = Usuario.UserName;
            txtClave.Text = Usuario.Clave;
            txtNombre.Text = Usuario.Nombre;
            cbxRol.SelectedValue = Usuario.RolId;
            cbxEstado.SelectedValue = Usuario.Estado;
        }

        #endregion

        #region Event
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Agregar();
                MetroMessageBox.Show(this, "Los datos se guardaron correctamente.", "Usuario", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MetroMessageBox.Show(this, ex.Message, "Usuario", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                Inactivar();
                MetroMessageBox.Show(this, "Los datos se eliminaron correctamente.", "Usuario", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MetroMessageBox.Show(this, ex.Message, "Usuario", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AddUser_Load(object sender, EventArgs e)
        {
            ReverseMap();
        }

        #endregion
    }
}
