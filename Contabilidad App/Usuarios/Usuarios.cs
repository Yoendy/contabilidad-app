﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using Service.Usuarios;
using DomainModel;

namespace Contabilidad_App.Usuarios
{
    public partial class Usuarios : MetroUserControl
    {
        private IUsuarioService _usuarioService;
        public Usuarios()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            var filtro = txtFiltro.Text;
            var monedas = _usuarioService.Search(filtro);
            CargarDatos(monedas);
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            var formulario = new AddUser();
            formulario.ShowDialog();
        }

        private void CargarDatos(IEnumerable<Usuario> usuarios)
        {
            dgvMonedas.Rows.Clear();
            foreach (var item in usuarios)
            {
                var estado = item.Estado ? "Activo" : "Inactivo";
                dgvMonedas.Rows.Add(item.Id, item.UserName, item.Nombre, item.Rol.Nombre, estado);
            }

        }

        private Usuario GetUsuario()
        {
            var usuario = new Usuario();
            var row = dgvMonedas.SelectedRows[0];
            if (row.Cells[0].Value == null)
                return usuario;

            usuario = _usuarioService.GetOne(Convert.ToInt32(row.Cells[0].Value.ToString()));
            return usuario;
        }

        private void Reload()
        {
            CargarDatos(_usuarioService.GetAll());
        }

        private void Usuarios_Load(object sender, EventArgs e)
        {
            _usuarioService = new UsuarioService();
            var usuarios = _usuarioService.GetAll();
            CargarDatos(usuarios);
        }


        private void dgvMonedas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var usuario = GetUsuario();
            var formulario = new AddUser { Text = "Editar Usuario", Usuario = usuario };
            var resultado = formulario.ShowDialog();

            if (resultado == DialogResult.OK)
            {
                Reload();
                dgvMonedas.Refresh();
            }
        }
    }
}
