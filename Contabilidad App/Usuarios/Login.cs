﻿using MetroFramework;
using MetroFramework.Forms;
using Service.Usuarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contabilidad_App.Usuarios
{
    public partial class Login : MetroForm
    {
        public Login()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            var _usuarioService = new UsuarioService();
            var usuario = txtUsuario.Text;
            var clave = txtClave.Text;


            try
            {
                //MetroMessageBox.Show(this, "Los datos se eliminaron correctamente.", "Auxiliar", MessageBoxButtons.OK,MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;

                if ( _usuarioService.Login(usuario, clave))
                {
                    var user = _usuarioService.GetSingle(x => x.UserName.Equals(usuario));
                    Utils.UsuarioActual.Usuario = user;
                    CleanTextBox();
                    var Menu = new Home();
                    Menu.Location = this.Location;
                    Menu.FormClosing += delegate { this.Show(); };
                    Menu.Show();
                    this.Hide();

                }
                else
                {
                    throw new Exception("El usuario o la clave no es valida.");
                }

            }
            catch (Exception ex)
            {
                MetroMessageBox.Show(this, ex.Message, "Login", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void CleanTextBox()
        {
            txtClave.Clear();
            txtUsuario.Clear();
        }
    }
}
