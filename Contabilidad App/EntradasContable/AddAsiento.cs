﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Contabilidad_App.Utils;
using DomainModel;
using MetroFramework;
using MetroFramework.Forms;
using Service.Auxiliares;
using Service.CuentasContables;
using Service.EntradasContables;
using Service.Monedas;
using Service.TiposMovimientos;

namespace Contabilidad_App.EntradasContable
{
    public partial class AddAsiento : MetroForm
    {
        public EntradaContable Asiento { get; set; }
        private readonly IEntradaContableService _entradaContableService;
        private readonly ITipoMonedaService _monedaService;
        private readonly IAuxiliarService _auxiliarService;
        private readonly ICuentaContableService _cuentaContableService;
        private readonly ITipoMovimientoService _tipoMovimientoService;

        public AddAsiento()
        {
            _entradaContableService = new EntradaContableService();
            _monedaService = new TipoMonedaService();
            _auxiliarService = new AuxiliarService();
            _cuentaContableService = new CuentaContableService();
            _tipoMovimientoService = new TipoMovimientoService();

            InitializeComponent();
            Cargar();
        }

        #region Cargar

        private void Cargar()
        {
            CargarEstado();
            CargarMonedas();
            CargarAuxiliares();
            CargarCuentas();
            CargarTiposMovimientos();
        }

        private void CargarEstado()
        {
            var estados = new Dictionary<bool, string> { { true, "Activo" }, { false, "Inactivo" } };
            cbxEstado.DataSource = new BindingSource(estados, null);
            cbxEstado.DisplayMember = "Value";
            cbxEstado.ValueMember = "Key";
        }

        private void CargarMonedas()
        {
            var monedas = _monedaService.GetAll().Select(x => new { x.Id, Nombre = x.Descripcion });
            cbxMoneda.DataSource = new BindingSource(monedas, null);
            cbxMoneda.DisplayMember = "Nombre";
            cbxMoneda.ValueMember = "Id";
        }

        private void CargarAuxiliares()
        {
            var auxiliares = _auxiliarService.GetAll().Select(x => new { x.Id, Nombre = x.Descripcion });
            cbxAuxiliar.DataSource = new BindingSource(auxiliares, null);
            cbxAuxiliar.DisplayMember = "Nombre";
            cbxAuxiliar.ValueMember = "Id";

        }

        private void CargarCuentas()
        {
            var cuentas = _cuentaContableService.GetAll(x => x.PermiteTransacciones)
                .Select(x => new { x.Id, Nombre = x.Descripcion });
            cbxCuenta.DataSource = new BindingSource(cuentas, null);
            cbxCuenta.DisplayMember = "Nombre";
            cbxCuenta.ValueMember = "Id";

        }

        private void CargarTiposMovimientos()
        {
            var tiposMovimientos = _tipoMovimientoService.GetAll();
            cbxTipoMovimientos.DataSource = new BindingSource(tiposMovimientos, null);
            cbxTipoMovimientos.DisplayMember = "Nombre";
            cbxTipoMovimientos.ValueMember = "Id";

        }

        #endregion

        #region CRUD

        private void Add(EntradaContable asiento)
        {
            _entradaContableService.Save(asiento);
        }

        #endregion

        #region Action

        private void Agregar()
        {
            var asiento = Map;
            Add(asiento);

        }

        private void Inactivar()
        {
            var tipoCuenta = Map;
            _entradaContableService.Delete(tipoCuenta.Id);
        }

        private EntradaContable Map
        {
            get
            {
                var model = new EntradaContable()
                {
                    Id = int.Parse(txtId.Text),
                    Descripcion = txtDescripcion.Text,
                    Fecha = txtFecha.Value,
                    AuxiliarId = Convert.ToInt32(cbxAuxiliar.SelectedValue),
                    MonedaId = Convert.ToInt32(cbxMoneda.SelectedValue),
                    Estado = (bool) cbxEstado.SelectedValue,
                };
                model.Tasa = !string.IsNullOrEmpty(txtTasa.Text) ? Convert.ToDecimal(txtTasa.Text) : 0;

                foreach (DataGridViewRow row in dvgDetalles.Rows)
                {
                    var item = new DetalleEntrada
                    {
                        Id = Convert.ToInt32(row.Cells[5].Value.ToString()),
                        EntradaContableId = model.Id,
                        CuentaId = Convert.ToInt32(row.Cells[0].Value.ToString()),
                        Debito = Convert.ToDecimal(row.Cells[2].Value.ToString()),
                        Credito = Convert.ToDecimal(row.Cells[3].Value.ToString())
                    };

                    model.Detalles.Add(item);

                }

                return model;
            }
        }

        private void ReverseMap()
        {
            if (Asiento == null) return;

            txtId.Text = Asiento.Id.ToString();
            txtDescripcion.Text = Asiento.Descripcion;
            txtFecha.Value = Asiento.Fecha;
            txtTasa.Text = Asiento.Tasa.ToString(CultureInfo.InvariantCulture);
            cbxAuxiliar.SelectedValue = Asiento.AuxiliarId;
            cbxMoneda.SelectedValue = Asiento.MonedaId;
            cbxEstado.SelectedValue = Asiento.Estado;


            foreach (var item in Asiento.Detalles)
            {
                dvgDetalles.Rows.Add(item.CuentaId, item.Cuenta?.Descripcion, item.Debito , item.Credito,"",item.Id);
            }

        }

        #endregion

        #region Event

        private void AddAsiento_Load(object sender, EventArgs e)
        {
            ReverseMap();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Agregar();
                MetroMessageBox.Show(this, "Los datos se guardaron correctamente.", "Asiento Contable",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MetroMessageBox.Show(this, ex.Message, "Asiento Contable", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                Inactivar();
                MetroMessageBox.Show(this, "Los datos se eliminaron correctamente.", "Asiento Contable",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MetroMessageBox.Show(this, ex.Message, "Asiento Contable", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbxMoneda_SelectedValueChanged(object sender, EventArgs e)
        {
          
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            var credito = Convert.ToDecimal(0);
            var debito = Convert.ToDecimal(0);
            var tipoMovimiento = Convert.ToInt32(cbxTipoMovimientos.SelectedValue);

            switch (tipoMovimiento)
            {
                case 1:
                    debito = Convert.ToDecimal(txtMonto.Text);
                    break;
                case 2:
                    credito = Convert.ToDecimal(txtMonto.Text);
                    break;
            }

            dvgDetalles.Rows.Add(cbxCuenta.SelectedValue, cbxCuenta.Text, debito, credito,"", txtDetalleAsientoId.Text);
            txtDetalleAsientoId.Text = $"0";
        }

        private void dvgDetalles_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 4)
            {
                EliminarRow(e.RowIndex);
            }

        }

        private void EliminarRow(int rowIndex)
        {
            dvgDetalles.Rows.RemoveAt(rowIndex);
        }

        private void txtMonto_KeyPress(object sender, KeyPressEventArgs e)
        {
            var validator = new TextBoxValidator();
            validator.IsDecimal(sender, e);
        }

        private void dvgDetalles_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var row = dvgDetalles.SelectedRows[0];
            if (row.Cells[0].Value == null)
                return;
            cbxCuenta.SelectedValue = row.Cells[0].Value;
            cbxTipoMovimientos.SelectedValue = Convert.ToInt32(row.Cells[2].Value) != 0 ? 1 : 2;
            txtMonto.Text = Convert.ToInt32(row.Cells[2].Value) != 0
                ? (row.Cells[2].Value).ToString()
                : (row.Cells[3].Value).ToString();

            txtDetalleAsientoId.Text = Convert.ToString(row.Cells[5].Value);

            EliminarRow(e.RowIndex);
        }

        #endregion

    }
}
