﻿using System.Windows.Forms;
using MetroFramework.Controls;

namespace Contabilidad_App.Utils
{
    public class TextBoxValidator
    {
        public void IsDecimal(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
            // Only allow one decimal

            var metroTextBox = sender as MetroTextBox;
            if (metroTextBox != null && ((e.KeyChar == '.') && (metroTextBox.Text.IndexOf('.') > -1)))
            {
                e.Handled = true;
            }
        }

        public void IsInteger(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }

}
