﻿namespace DomainModel.Base
{
    public class BaseDomainModel
    {
        public int Id { get; set; }
        public bool Estado { get; set; } = true;
    }
}
