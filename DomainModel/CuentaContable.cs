using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DomainModel.Base;

namespace DomainModel
{
    [Table("CuentaContable")]
    public class CuentaContable : BaseDomainModel
    {
        [Required]
        [StringLength(100)]
        public string Descripcion { get; set; }

        [Required]
        public bool PermiteTransacciones { get; set; }

        [Required]
        public int Nivel { get; set; } = 1;

        [Required]
        [StringLength(50)]
        public string NoCuenta { get; set; }

        
        public int? CuentaMayorId { get; set; }

        [Required]
        public decimal Balance { get; set; } = 0;

        [Required]
        public int NoControl { get; set; } = 1;

        [Required]
        public int TipoCuentaId { get; set; }

        #region Relations

        public virtual TipoCuenta TipoCuenta { get; set; }
        public virtual CuentaContable CuentaMayor { get; set; }

        public ICollection<DetalleEntrada> Detalles;

        public IEnumerable<CuentaContable> CuentasAsociadas;

        #endregion

    }
}
