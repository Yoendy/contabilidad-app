using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DomainModel.Base;

namespace DomainModel
{

    [Table("DetalleEntrada")]
    public  class DetalleEntrada : BaseDomainModel
    {
        [Required]
        public int CuentaId { get; set; }

        [Required]
        public decimal Credito { get; set; } = 0;

        [Required]
        public decimal Debito { get; set; } = 0;

        public int EntradaContableId { get; set; }

        #region Relations
        public virtual CuentaContable Cuenta { get; set; }

        public virtual EntradaContable EntradaContable { get; set; }
        #endregion
    }
}
