﻿using DomainModel.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel
{
    public class Rol : BaseDomainModel
    {
        [Required]
        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public ICollection<Usuario> Usuarios { get; set; }
    }
}
