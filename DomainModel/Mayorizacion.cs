using System;
using System.ComponentModel.DataAnnotations.Schema;
using DomainModel.Base;

namespace DomainModel
{
    [Table("Mayorizacion")]
    public class Mayorizacion :BaseDomainModel
    {

        [Column(TypeName = "date")]
        public DateTime Fecha { get; set; }

        public decimal Balance { get; set; }

        public int Cuenta { get; set; }

        public int TipoMovimiento { get; set; }
    }
}
