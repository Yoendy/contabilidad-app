using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DomainModel.Base;

namespace DomainModel
{
    [Table("EntradaContable")]
    public class EntradaContable : BaseDomainModel
    {
        public EntradaContable()
        {
            Detalles = new List<DetalleEntrada>();
        }

        [Required]
        public string Descripcion { get; set; }

        [Required]
        public DateTime Fecha { get; set; }

        [Required]
        public decimal Tasa { get; set; } = 1;

        [Required]
        public int AuxiliarId { get; set; } = 1;

        [Required]
        public int MonedaId { get; set; } = 1;

        [Required]
        public string Estatus { get; set; } = "R"; // R: Registrado, M: Mayorizado

        #region Relations
        public virtual Auxiliar Auxiliar { get; set; }
        public virtual TipoMoneda Moneda { get; set; }

        public ICollection<DetalleEntrada> Detalles { get; set; }
        #endregion

    }
}
