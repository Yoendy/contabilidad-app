﻿using DomainModel.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel
{
    public class Usuario : BaseDomainModel
    {

        [Required]
        //[Index(IsUnique = true)]
        public string UserName { get; set; }

        [Required]
        public string Clave { get; set; }

        [Required]
        public String Nombre { get; set; }

        public int RolId { get; set; }

        public  virtual Rol Rol { get; set; }
}
}
