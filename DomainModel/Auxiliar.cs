using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DomainModel.Base;

namespace DomainModel
{

    [Table("Auxiliar")]
    public  class Auxiliar : BaseDomainModel
    {
        [Required]
        [StringLength(50)]
        public string Descripcion { get; set; }

        public ICollection<EntradaContable> EntradasContables { get; set; }
    }
}
