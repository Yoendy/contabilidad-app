﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Repository
{
    public interface IGenericRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        T GetOne(int id);
        IEnumerable<T> GetAll(Expression<Func<T, bool>> predicate);
        T GetSingle(Expression<Func<T, bool>> predicate);
        IQueryable<T> Query(Expression<Func<T, bool>> predicate);
        long Count(Expression<Func<T, bool>> predicate);
        long Count();
        bool Exists(int id);
        bool Exists(Expression<Func<T, bool>> predicate);

    }
}
