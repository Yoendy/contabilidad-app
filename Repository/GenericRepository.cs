﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using DomainModel.Base;

namespace Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseDomainModel
    {
        private bool _disposed;
        internal ContabilidadContext _db { get; }
        internal DbQuery<T> Entities;

        public GenericRepository()
        {
            _db = new ContabilidadContext();
            Entities = _db.Set<T>().AsNoTracking();
        }

        private IQueryable<T> FindBy()
        {
            return Entities.Where(x => x.Estado);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return FindBy().ToList();
        }

        public virtual void Add(T entity)
        {
            _db.Entry(entity).State = EntityState.Added;
            try
            {
                _db.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                Exception raise = null;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        raise = dbEx;
                        var message = $"{validationErrors.Entry.Entity.ToString()}:{validationError.ErrorMessage}";
                        raise = new InvalidOperationException(message, raise);
                    }
                    
                }

                if (raise != null)
                {
                    throw raise;
                }

            }
        }

        public virtual void Delete(T entity)
        {
            _db.Entry(entity).State = EntityState.Deleted;
            _db.SaveChanges();
        }

        public virtual void Update(T entity)
        {
            //Magic Method
            var model = GetOne(entity.Id);
            _db.Set<T>().Attach(model);
            _db.Entry(model).CurrentValues.SetValues(entity);
            try
            {
                _db.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                Exception raise = null;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        raise = dbEx;
                        var message = $"{validationErrors.Entry.Entity.ToString()}:{validationError.ErrorMessage}";
                        raise = new InvalidOperationException(message, raise);
                    }

                }

                if (raise != null)
                {
                    throw raise;
                }

            }
        }

        public bool Exists(int id)
        {
            return Exists(m => m.Id == id );
        }

        public bool Exists(Expression<Func<T, bool>> predicate)
        {
            return FindBy().Any(predicate);
        }

        public virtual T GetOne(int id)
        {
            return GetSingle(x => x.Id == id);
        }

        public virtual T GetSingle(Expression<Func<T, bool>> predicate)
        {
            return FindBy().FirstOrDefault(predicate);
        }

        public IEnumerable<T> GetAll(Expression<Func<T, bool>> predicate)
        {
            return Query(predicate);
        }

        public IQueryable<T> Query(Expression<Func<T, bool>> predicate)
        {
            return FindBy().Where(predicate);
        }

        public long Count(Expression<Func<T, bool>> predicate)
        {
            return Query(predicate).Count();
        }

        public long Count()
        {
            return FindBy().Count();
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
